using Toybox.WatchUi;
using Toybox.Graphics;
using Toybox.UserProfile;
using Toybox.Math;
using Toybox.System;
using Toybox.Time;


class ColorHearRateZonesChartView extends WatchUi.DataField {
    
    var curHR;
    var hrValues;
    var fullscreen;
    var fontSize;
    var fontYShift;
    const hrZoneInfo = UserProfile.getHeartRateZones(UserProfile.getCurrentSport());
    const hrZoneCol = [
    	0x5c5c5c,
    	0x3badff,
    	0xb4ff3b,
    	0xf5e536,
    	0xffb03b,
    	0xFC5B57,
    	0x9f0b11
    ];
    const angle = Math.toRadians(40);
    const propH = 0.75;
    const propL = 0.25;
    const devScrWidth = System.getDeviceSettings().screenWidth;
    
    
    function initialize() {
        DataField.initialize();
        hrValues = new [Math.floor(Math.cos(angle)*devScrWidth*propH)-2];
        for(var i = 0; i < hrValues.size(); i++){
            hrValues[i] = 84+(i%110);
		}
        curHR = 0;
    }


    function getHRColor(hrValue) {
    	var i = 0;
    	while(i < hrZoneInfo.size()){
			if (hrValue <= hrZoneInfo[i]){
				break;
			}
			i++;
		}
    	return hrZoneCol[i];
    }

    function drawGraphPlane(dc, startX, startY, width, height) {
        var pWidth = Math.floor(Math.cos(angle)*width); // half of the plane width
        var pHeight = Math.floor(Math.sin(angle)*height); // half of the plane height
        var x = startX;
        var y = startY;
        var zoneY = 0;
        if (getBackgroundColor() == Graphics.COLOR_BLACK){
            dc.setColor(
                Graphics.COLOR_WHITE,
                Graphics.COLOR_TRANSPARENT
            );
        }
        else {
            dc.setColor(
                Graphics.COLOR_BLACK,
                Graphics.COLOR_TRANSPARENT
            );
        }
        dc.drawRectangle(
        	x,
            y,
           	pWidth,
            pHeight
        );
        //  graph height divided by the HR range we want to present on the graph to calculate how many pixels go for single heartbeat
        var hrHeightFactor = pHeight / ((hrZoneInfo[hrZoneInfo.size()-1] - hrZoneInfo[0]) + 20);
        for(var i = 0; i < hrZoneInfo.size(); i++){
            zoneY = y+pHeight-(hrZoneInfo[i]-hrZoneInfo[0]+10)*hrHeightFactor-1;
            dc.drawLine(
                x-5, 
                zoneY,
                x,
                zoneY
            );
            if (fullscreen){
                dc.drawText(
                    x-10,
                    zoneY,
                    Graphics.FONT_SYSTEM_XTINY, 
                    hrZoneInfo[i], 
                    Graphics.TEXT_JUSTIFY_RIGHT | Graphics.TEXT_JUSTIFY_VCENTER
                );
            }
        }
        return [x, y, pWidth, pHeight, hrHeightFactor];
    }

    function drawGraphSpike(dc, value, color, shift, plane){
        // substract borders from plane width
        var widthFactor = (plane[2]-2)/hrValues.size();
        var spikeWidth = 1;
        dc.setColor(
	            color,
	            color
	        );
	    var spikeX = 1+plane[0]+spikeWidth*shift*widthFactor;
	    var spikeHeight = Math.ceil((value-(hrZoneInfo[0]-10))*plane[4]);
	    var spikeY = plane[1]+(plane[3]-spikeHeight)-1;
        dc.fillRectangle(
            spikeX,
            spikeY,
            spikeWidth,
            spikeHeight
        );
    }

    // The given info object contains all the current workout information.
    // Calculate a value and save it locally in this method.
    // Note that compute() and onUpdate() are asynchronous, and there is no
    // guarantee that compute() will be called before onUpdate().
    function compute(info) {
        // See Activity.Info in the documentation for available information.
        if(info has :currentHeartRate){
        	curHR = 0;
            if(info.currentHeartRate != null){
                curHR = info.currentHeartRate;
            } 
            for(var i = 0; i < hrValues.size()-1; i++){
                hrValues[i] = hrValues[i+1];
            }
            hrValues[hrValues.size()-1] = curHR;
        }
    }

    // Set your layout here. Anytime the size of obscurity of
    // the draw context is changed this will be called.
    function onLayout(dc) {
        var obscurityFlags = DataField.getObscurityFlags();
        fullscreen = false;
        fontSize = Graphics.FONT_XTINY;
        fontYShift = 10;
		if (obscurityFlags == (OBSCURE_BOTTOM|OBSCURE_RIGHT|OBSCURE_LEFT|OBSCURE_TOP)){
            View.setLayout(Rez.Layouts.MainLayout(dc));
            fullscreen = true;
            fontSize = Graphics.FONT_NUMBER_MEDIUM;
            fontYShift = 20;
        }
        
		return true;
        
    }

    // Display the value you computed here. This will be called
    // once a second when the data field is visible.
    function onUpdate(dc) {
        // Set the background color
		var plane = drawGraphPlane(dc, dc.getWidth()*propL, dc.getHeight()*propL, dc.getWidth()*propH, dc.getHeight()*propH);
        var widthFactor = (hrValues.size()/(plane[2]-2)).toNumber();
        for(var i = 0; i < hrValues.size(); i++){
        	var test = i % widthFactor;
            if (i % widthFactor == 0){
	        	var color = getHRColor(hrValues[i]);
	            drawGraphSpike(dc, hrValues[i], color, i, plane);
            }
		}
        dc.setColor(
            getHRColor(curHR),
            Graphics.COLOR_TRANSPARENT
        );
        dc.drawText(
            dc.getWidth()/2,
            plane[1]+plane[3]+fontYShift,
            fontSize,
            curHR,
            Graphics.TEXT_JUSTIFY_CENTER | Graphics.TEXT_JUSTIFY_VCENTER
        );

        // Call parent's onUpdate(dc) to redraw the layout
        dc.draw();
        View.onUpdate(dc);
    }

}
